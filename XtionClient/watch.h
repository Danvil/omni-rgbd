#include <time.h>

class watch{
	timespec start;
	timespec stop;

	public:
		void tic();
		timespec toc();
};