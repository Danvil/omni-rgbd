/*
 * This file is part of the OpenKinect Project. http://www.openkinect.org
 *
 * Copyright (c) 2010 individual OpenKinect contributors. See the CONTRIB file
 * for details.
 *
 * This code is licensed to you under the terms of the Apache License, version
 * 2.0, or, at your option, the terms of the GNU General Public License,
 * version 2.0. See the APACHE20 and GPL2 files for the text of the licenses,
 * or the following URLs:
 * http://www.apache.org/licenses/LICENSE-2.0
 * http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * If you redistribute this file in source form, modified or unmodified, you
 * may:
 *   1) Leave this header intact and distribute it under the same terms,
 *      accompanying it with the APACHE20 and GPL20 files, or
 *   2) Delete the Apache 2.0 clause and accompany it with the GPL2 file, or
 *   3) Delete the GPL v2 clause and accompany it with the APACHE20 file
 * In all cases you must keep the copyright notice intact and include a copy
 * of the CONTRIB file.
 *
 * Binary distributions must follow the binary distribution requirements of
 * either License.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <pthread.h>

#if defined(__APPLE__)
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <math.h>

#include "network.h"
#include "watch.h"

#include <snappy.h>
std::string snappyin = "Hallo 123 test";
std::string snappyout = "Hallo 321 test";

#define USEUDP

#if defined(USETCP)
	TCPClient con1;
#endif
#if defined(USEUDP)
	UDPSock udpcon;
#endif

watch exittimer; 

volatile int die = 0;
volatile int wanttodie = 0;

int g_argc;
char **g_argv;

int window;

//FILE *fp;

uint32_t xres = 320;
uint32_t yres = 240;

pthread_t thread;
pthread_t glthread;
pthread_mutex_t gl_backbuf_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t gl_frame_cond = PTHREAD_COND_INITIALIZER;

// back: owned by libfreenect (implicit for depth)
// mid: owned by callbacks, "latest frame ready"
// front: owned by GL, "currently being drawn"
uint8_t *depth_mid, *depth_front;
uint16_t *rcvbuf;
uint16_t *depthmap;

GLuint gl_depth_tex;

int got_depth = 0;



void DrawGLScene()
{
	pthread_mutex_lock(&gl_backbuf_mutex);

	while (!got_depth ) {
		pthread_cond_wait(&gl_frame_cond, &gl_backbuf_mutex);
	}

	uint8_t *tmp;

	if (got_depth) {
		tmp = depth_front;
		depth_front = depth_mid;
		depth_mid = tmp;
		got_depth = 0;
	}

	pthread_mutex_unlock(&gl_backbuf_mutex);

	glBindTexture(GL_TEXTURE_2D, gl_depth_tex);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, xres, yres, 0, GL_RGB, GL_UNSIGNED_BYTE, depth_front);

	glBegin(GL_TRIANGLE_FAN);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glTexCoord2f(0, 0); glVertex3f(0,0,0);
	glTexCoord2f(1, 0); glVertex3f(xres,0,0);
	glTexCoord2f(1, 1); glVertex3f(xres,yres,0);
	glTexCoord2f(0, 1); glVertex3f(0,yres,0);
	glEnd();

	glutSwapBuffers();
}

void keyPressed(unsigned char key, int x, int y)
{
	if (key == 27) {
		die = 1;
		pthread_join(thread, NULL);
		glutDestroyWindow(window);
		free(depth_mid);
		free(depth_front);
		free(rcvbuf);
		//fclose(fp);	
		exit(0);
	}
	
}

void ReSizeGLScene(int Width, int Height)
{
	glViewport(0,0,Width,Height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho (0, xres, yres, 0, -1.0f, 1.0f);
	glMatrixMode(GL_MODELVIEW);
    	glLoadIdentity();
}

void InitGL(int Width, int Height)
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClearDepth(1.0);
	glDepthFunc(GL_LESS);
    	glDepthMask(GL_FALSE);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
    	glDisable(GL_ALPHA_TEST);
    	glEnable(GL_TEXTURE_2D);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glShadeModel(GL_FLAT);

	glGenTextures(1, &gl_depth_tex);
	glBindTexture(GL_TEXTURE_2D, gl_depth_tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	ReSizeGLScene(Width, Height);
}

void *gl_threadfunc(void *arg)
{
	printf("GL thread\n");

	glutInit(&g_argc, g_argv);
	printf("glutInit successfull\n");


	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH);
	glutInitWindowSize(xres, yres);
	glutInitWindowPosition(1800, 0);

	window = glutCreateWindow("Client");
	printf("glutCreateWindow successfull\n");

	glutDisplayFunc(&DrawGLScene);
	glutIdleFunc(&DrawGLScene);
	glutReshapeFunc(&ReSizeGLScene);
	glutKeyboardFunc(&keyPressed);

	InitGL(xres, yres);

	glutMainLoop();

	return NULL;
}


void translateDepthMap(void *v_depth)
{
	uint32_t i;
	uint16_t *depth = (uint16_t*)v_depth;

	pthread_mutex_lock(&gl_backbuf_mutex);
	
	for (i=0; i<xres*yres; i++) {
		depth_mid[3*i+0] = 0;
		depth_mid[3*i+1] = 0;
		depth_mid[3*i+2] = 255-255*((float)depth[i]/10000.0f);
		if(depth[i] == 0)
			depth_mid[3*i+2] = 0;
	}

	got_depth++;
	pthread_cond_signal(&gl_frame_cond);
	pthread_mutex_unlock(&gl_backbuf_mutex);
}

void *threadfunc(void *arg)
{
	watch timer1;
	uint32_t packlength = 0;	
	float fps=0;

#if defined(USEUDP)
	uint16_t framebuf[xres*yres];
	uint16_t fragnum = 10;
	uint32_t headersizebytes = 6;
	uint32_t fragsizeshorts = xres*yres/fragnum;
	uint32_t fragsizebytes = fragsizeshorts << 1;
#endif


	while (!die) {
	
#if defined(USETCP)
		con1.receive(&packlength, sizeof(uint32_t));
		con1.receive(rcvbuf, packlength);
		long time = 0;
		con1.receive(&time, sizeof(long));
		fps = 0;
		con1.receive(&fps, sizeof(float));
		
		//copying compressed image from buffer to string
		snappyin.assign((const char*)rcvbuf, packlength);
		//decompress data in string	
		snappy::Uncompress(snappyin.data(), packlength, &snappyout);

		depthmap = (uint16_t*)snappyout.data();
#endif
#if defined(USEUDP)
		//Wait in this loop until fragnum packets arrived. (one frame)
		int p=0;
		for(p=0; p < fragnum; p++){
			//read compressed image using rcvbuf as buffer
			packlength = udpcon.receive(rcvbuf, xres*yres*2);
		
			//copying compressed image from buffer to string
			snappyin.assign((const char*)rcvbuf, packlength);
			//decompress data in string	
			snappy::Uncompress(snappyin.data(), packlength, &snappyout);

			//get fragment header
			uint32_t frameid = *((uint32_t*)snappyout.data());
			uint16_t fragid = *(snappyout.data()+4);

			//get fragment data
			memcpy(&framebuf[fragid*fragsizeshorts], snappyout.data()+headersizebytes, fragsizebytes);
		}
		depthmap = framebuf;
#endif

		//timespec timesincelast = timer1.toc();
		//long nsecsincelast = timesincelast.tv_nsec;
		//long secsincelast = timesincelast.tv_sec;
		//fps = 1e9/nsecsincelast;	

		//timer1.tic();
		//if(wanttodie == 0)
		//	fprintf(fp, "%ld,%ld,%6f;\n", secsincelast, nsecsincelast, fps);
	
		translateDepthMap(depthmap);


		//if(exittimer.toc().tv_sec == 60*10){
		//	printf("Messung fertig\n");
		//	wanttodie=1;
		//}
	}


	printf("exit\n");
	pthread_exit(0);
}

int main(int argc, char **argv)
{
	exittimer.tic();

    	if (argc < 3) {
       		fprintf(stderr,"usage %s hostname port\n", argv[0]);
       		exit(0);
    	}


	//fp=fopen("messungen.txt", "w");


#if defined(USETCP)
	con1.init();
	con1.conconnect(argv[1], atoi(argv[2]));
#endif
#if defined(USEUDP)	
	udpcon.init(atoi(argv[2]));
	udpcon.setpartner(argv[1], atoi(argv[2]));
#endif
    	printf("network init done\n");
		

	depth_mid = (uint8_t*)malloc(xres*yres*3);
	depth_front = (uint8_t*)malloc(xres*yres*3);
	rcvbuf = (uint16_t*)malloc(xres*yres*2);


	g_argc = argc;
	g_argv = argv;

	int res;
	res = pthread_create(&thread, NULL, threadfunc, NULL);
	if (res) {
		printf("pthread_create failed\n");
		return 1;
	}
	printf("pthread created\n");

	
	// OS X requires GLUT to run on the main thread
	gl_threadfunc(NULL);


	return 0;
}
