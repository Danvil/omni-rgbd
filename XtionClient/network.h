#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>


class TCPServer{

	int sockfd;
	int newsockfd;

	public:
		~TCPServer();
		void init(int portno);
		void conaccept();
		void send(void* data, size_t numbytes);
		void receive(void* data, size_t numbytes);
		void conclose();

	private:
		void error(const char *msg);
};



#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

class TCPClient{

	int sockfd;
	struct hostent *server;
	struct sockaddr_in serv_addr;

	public:
		~TCPClient();	
		void init();
		void conconnect(const char *name, int portno);
		void send(void* data, size_t numbytes);
		void receive(void* data, size_t numbytes);
		void conclose();

	private:
		void error(const char *msg);
};



#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <netdb.h>
#include <stdio.h>

class UDPSock{

	int sockfd;
	struct sockaddr_in here;
	struct sockaddr_in partner;

	public:
		void init(int portno);
		void setpartner(const char *name, int portno);
		void send(void *data, size_t numbytes);
		unsigned int receive(void *data, size_t numbytes);

	private:
		void error(const char *msg);
	
};







