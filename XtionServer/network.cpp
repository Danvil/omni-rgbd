#include "network.h"


TCPServer::~TCPServer(){
	conclose();
}

void TCPServer::error(const char *msg)
{
    perror(msg);
    exit(1);
}


void TCPServer::init(int portno){

	struct sockaddr_in serv_addr;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) 
		error("ERROR opening socket");

	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);

	if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0){
		close(sockfd); 
		error("ERROR on binding");
	}

	listen(sockfd,5);

}

void TCPServer::conaccept(){

	struct sockaddr_in cli_addr;
	socklen_t clilen;

	clilen = sizeof(cli_addr);
	newsockfd = accept(sockfd,(struct sockaddr *) &cli_addr, &clilen);
	if (newsockfd < 0) 
        	error("ERROR on accept");

}

void TCPServer::send(void* data, size_t numbytes){
	
	int n = write(newsockfd, data, numbytes);
	if (n < 0) error("ERROR writing to socket");
	if (n != (int)numbytes) printf("only %d bytes written\n", n);

}

void TCPServer::receive(void *data, size_t numbytes){
	int n;
	int count = 0;
	while(count < (int)numbytes){
		n = read(newsockfd, ((char*)data)+count, numbytes-count);
		if (n < 0) error("ERROR reading from socket");
		count+=n;
	}
}

void TCPServer::conclose(){
	
	close(sockfd);
	close(newsockfd);
}


////////////////////////////////////////////////////////////////////////////



TCPClient::~TCPClient(){
	conclose();
}

void TCPClient::error(const char *msg)
{
    perror(msg);
    exit(1);
}

void TCPClient::init(){
	
    	sockfd = socket(AF_INET, SOCK_STREAM, 0);
    	if (sockfd < 0) 
        	error("ERROR opening socket");
    	
}


void TCPClient::conconnect(const char *name, int portno){
	server = gethostbyname(name);
    	if (server == NULL) {
        	fprintf(stderr,"ERROR, no such host\n");
        	exit(0);
    	}

    	bzero((char *)&serv_addr, sizeof(serv_addr));
    	serv_addr.sin_family = AF_INET;
    	bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
    	serv_addr.sin_port = htons(portno);
    	if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        	error("ERROR connecting");
}

void TCPClient::send(void *data, size_t numbytes){

	int n = write(sockfd, data, numbytes);
	if (n < 0) error("ERROR writing to socket");
	if (n != (int)numbytes) printf("only %d bytes written\n", n);

}

void TCPClient::receive(void *data, size_t numbytes){
	int n;
	int count = 0;
	while(count < (int)numbytes){
		n = read(sockfd, ((char*)data)+count, numbytes-count);
		if (n < 0) error("ERROR reading from socket");
		count+=n;
	}
}

void TCPClient::conclose(){
	
	close(sockfd);
}




//////////////////////////////////////////////////////////////////////7
//////////////////////////////////////////////////////////////////////
//UDP


void UDPSock::error(const char *msg)
{
    perror(msg);
    exit(1);
}

void UDPSock::init(int portno){

	sockfd=socket(AF_INET, SOCK_DGRAM, 0);
	if (sockfd < 0) error("Opening socket");

	//filling server struct
	bzero(&here, sizeof(here));
	here.sin_family=AF_INET;
	here.sin_addr.s_addr=INADDR_ANY;
	here.sin_port=htons(portno);

	if (bind(sockfd,(struct sockaddr *)&here, sizeof(here))<0) 
		error("binding");

}

void UDPSock::receive(void *data, size_t numbytes){
	int n;

	struct sockaddr_in from;
	unsigned int len = sizeof(from);

	n = recvfrom(sockfd, data, numbytes, 0, (struct sockaddr *)&from, &len);
	if(n < 0) error("recvfrom");

	if((n != (int)numbytes) || (from.sin_addr.s_addr != partner.sin_addr.s_addr)){
		printf("received missmatching datagram, waiting for next one\n");
		//get the next datagram
		receive(data, numbytes);
	}
}

void UDPSock::send(void* data, size_t numbytes){

	if( numbytes > 65507 ){
		printf("udp packet too long\n");
		return;
	}	

	int n;
	n = sendto(sockfd, data, numbytes, 0, (struct sockaddr *)&partner, sizeof(partner));
	if(n < 0) error("sendto");
}

void UDPSock::setpartner(const char *name, int portno){
	struct hostent *hp;
	hp = gethostbyname(name);

	bcopy((char*)hp->h_addr, (char*)&partner.sin_addr, hp->h_length);
	partner.sin_port = htons(portno);
	partner.sin_family = AF_INET; 
}






