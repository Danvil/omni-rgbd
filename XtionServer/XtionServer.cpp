/****************************************************************************
*                                                                           *
*  OpenNI 1.x Alpha                                                         *
*  Copyright (C) 2011 PrimeSense Ltd.                                       *
*                                                                           *
*  This file is part of OpenNI.                                             *
*                                                                           *
*  OpenNI is free software: you can redistribute it and/or modify           *
*  it under the terms of the GNU Lesser General Public License as published *
*  by the Free Software Foundation, either version 3 of the License, or     *
*  (at your option) any later version.                                      *
*                                                                           *
*  OpenNI is distributed in the hope that it will be useful,                *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of           *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the             *
*  GNU Lesser General Public License for more details.                      *
*                                                                           *
*  You should have received a copy of the GNU Lesser General Public License *
*  along with OpenNI. If not, see <http://www.gnu.org/licenses/>.           *
*                                                                           *
****************************************************************************/
//---------------------------------------------------------------------------
// Includes
//---------------------------------------------------------------------------
#include <XnOpenNI.h>
#include <XnLog.h>
#include <XnCppWrapper.h>
#include <XnFPSCalculator.h>

//#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>


#include "watch.h"
#include "network.h"

#include <snappy.h>


//---------------------------------------------------------------------------
// Defines
//---------------------------------------------------------------------------
//#define USE_RGB_IMAGE_STREAMS
#define USEUDP
//---------------------------------------------------------------------------
// Macros
//---------------------------------------------------------------------------
#define CHECK_RC(rc, what)                                      \
if (rc != XN_STATUS_OK)                                         \
{                                                               \
    printf("%s failed: %s\n", what, xnGetStatusString(rc));     \
    exit(1);                                                  \
}

#define CHECK_RC_CONTINUE(rc, what)                             \
if (rc != XN_STATUS_OK)                                         \
{                                                               \
 printf("%s failed: %s\n", what, xnGetStatusString(rc));        \
}
//---------------------------------------------------------------------------
// Code
//---------------------------------------------------------------------------

std::string snappyin = "testinput";
std::string snappyout = "testoutput";

uint32_t numsensors = 1;

typedef struct 
{
        char name[80];
        xn::ProductionNode device;
        xn::DepthGenerator depth;
        xn::DepthMetaData depthMD;
#if defined(USE_RGB_IMAGE_STREAMS)
        xn::ImageGenerator image;
        xn::ImageMetaData imageMD;
#endif
}DepthRgbSensors;


void UpdateCommon(xn::Context *g_Context, DepthRgbSensors *sensor, int numberofsensors)
{
        XnStatus rc = XN_STATUS_OK;
        //rc = g_Context->WaitAnyUpdateAll();
        //This function will wait until all nodes will have new data:
        rc = g_Context->WaitAndUpdateAll();
        CHECK_RC_CONTINUE(rc, "WaitAnyUpdateAll()");

	int k;
	for(k=0; k<numberofsensors; k++){
        	if (sensor[k].depth.IsValid())
                	sensor[k].depth.GetMetaData(sensor[k].depthMD);
	}
#if defined(USE_RGB_IMAGE_STREAMS)
        if (sensor->image.IsValid())
        {
                sensor.image.GetMetaData(sensor.imageMD);
        }
#endif
}


DepthRgbSensors* SensorsInit(xn::Context *g_Context, int numsensors)
{
	XnStatus nRetVal = XN_STATUS_OK;


	// Getting Sensors information and configure all sensors
	DepthRgbSensors *sensors = new DepthRgbSensors[numsensors];


	nRetVal = g_Context->Init();
	
	   
	xn::NodeInfoList devicesList;
	   
	nRetVal = g_Context->EnumerateProductionTrees(XN_NODE_TYPE_DEVICE, NULL, devicesList);
	CHECK_RC(nRetVal, "Enumerate");
	
	 
	//Count the number of detected devices and exit if requested number of sensors is higher than this number 	   
	int devicecount = 0;
	for (xn::NodeInfoList::Iterator it = devicesList.Begin(); it != devicesList.End(); ++it, devicecount++){}
	if( devicecount < numsensors ){
		printf("Error: only %d devices detected\n", devicecount);
		exit(1);
	}

	//initialize the requested amount of sensors
	int i=0;	
	for (xn::NodeInfoList::Iterator it = devicesList.Begin(); i<numsensors; ++it, ++i)
	{
		// Create the device node
	       	xn::NodeInfo deviceInfo = *it;
	       	nRetVal = g_Context->CreateProductionTree(deviceInfo, sensors[i].device);
	       	CHECK_RC(nRetVal, "Create Device");
	
	       	// Create a query to depend on this node
	       	xn::Query query;
	       	query.AddNeededNode(deviceInfo.GetInstanceName());
	
	       	// Copy the device name
	       	xnOSMemCopy(sensors[i].name,deviceInfo.GetInstanceName(), xnOSStrLen(deviceInfo.GetInstanceName()));
	       
		// Now create a depth generator over this device
	       	nRetVal = g_Context->CreateAnyProductionTree(XN_NODE_TYPE_DEPTH, &query, sensors[i].depth);
	       	CHECK_RC(nRetVal, "Create Depth");

#if defined(USE_RGB_IMAGE_STREAMS)
	        // now create a image generator over this device
	        nRetVal = g_Context->CreateAnyProductionTree(XN_NODE_TYPE_IMAGE, &query, sensors[i].image);
	        CHECK_RC(nRetVal, "Create Image");
#endif
	}


	//set output mode of all requested sensors	
	XnMapOutputMode mapMode;
	mapMode.nXRes = 320;
	mapMode.nYRes = 240;
	mapMode.nFPS = 30;

	for(i=0; i<numsensors; i++){
		nRetVal = sensors[i].depth.SetMapOutputMode(mapMode);
	}
	 
	return sensors;

}


int main(int argc, char *argv[])
{
#if defined(USETCP)	
	if (argc < 2) {
		fprintf(stderr,"ERROR, no port provided\n");
		exit(1);
	}

	if(argc == 3)
		numsensors = atoi(argv[2]);
#endif
#if defined(USEUDP)
	if (argc < 2) {
		fprintf(stderr,"ERROR, no port provided\n");
		exit(1);
	}
	if (argc < 3) {
		fprintf(stderr,"ERROR, no ip-address provided\n");
		exit(1);
	}
	if(argc == 4)
		numsensors = atoi(argv[3]);
#endif

#if defined(USETCP)
	TCPServer con[numsensors];
#endif
#if defined(USEUDP)
	UDPSock udpcon[numsensors];
#endif

	int k;
	for(k=0; k < numsensors; k++){
#if defined(USETCP)
		printf("Waiting for connection on port %d\n", atoi(argv[1])+k);
		fflush(stdout);
		con[k].init(atoi(argv[1])+k);
		con[k].conaccept();
		printf("Connection on port %d accepted\n", atoi(argv[1])+k);
#endif
#if defined(USEUDP)
		udpcon[k].init(atoi(argv[1])+k);
		udpcon[k].setpartner(argv[2], atoi(argv[1])+k);
#endif
	}

	
	XnStatus nRetVal = XN_STATUS_OK;

	xn::Context g_Context;

	DepthRgbSensors *sensors = SensorsInit(&g_Context, numsensors);

	g_Context.StartGeneratingAll();

	XnFPSData xnFPS;
	nRetVal = xnFPSInit(&xnFPS, 180);
	CHECK_RC(nRetVal, "FPS Init");
	
	uint32_t frameid = 0;

	while (!xnOSWasKeyboardHit())
	{

		//update data. This function is blocking. Will return when every camera has new data.
		UpdateCommon(&g_Context, sensors, numsensors);

		//Compress the data of all requested sensors and send it via network
		int i=0;
		for(i = 0 ; i < numsensors ; ++i)
	       	{
	           	xnFPSMarkFrame(&xnFPS);

           		const XnDepthPixel* pDepthMap = sensors[i].depthMD.Data();


			watch timer1;
			timer1.tic();			
			long time = timer1.toc().tv_nsec;
		
			float fps = xnFPSCalc(&xnFPS);
			printf("FPS:%6f\n", fps);

#if defined(USETCP)
			//COMPRESS
			//Copy pDepthMap into a std::string because snappy needs a string as input
			snappyin.assign((const char*)pDepthMap, sensors[i].depthMD.XRes()*sensors[i].depthMD.YRes()*2);
			//Compress data into the std::string snappyout
			snappy::Compress(snappyin.data(), snappyin.size(), &snappyout);

			//SEND
			uint32_t piclength = snappyout.size();	
			con[i].send(&piclength, sizeof(uint32_t));
			//send compressed data
			con[i].send((void*)snappyout.data(), snappyout.size());
			con[i].send(&time, sizeof(long));
			con[i].send(&fps, sizeof(float));
#endif
#if defined(USEUDP)	
			frameid++;
			uint16_t fragnum = 10; // picsizebytes%fragnum HAS TO BE ZERO !!!!!!
			uint16_t fragid = 0;
			uint32_t picsizebytes = sensors[i].depthMD.XRes()*sensors[i].depthMD.YRes()*2;
			uint32_t headersizebytes = sizeof(frameid)+sizeof(fragid);
			uint32_t fragsizebytes = (picsizebytes/fragnum);
			uint32_t fragsizeshorts = fragsizebytes>>1;

			for(fragid=0; fragid < fragnum; fragid++){
				//Merge Header and Fragment
				snappyin.assign((const char*)&frameid, sizeof(frameid));
				snappyin.append((const char*)&fragid, sizeof(fragid));
				snappyin.append((const char*)&pDepthMap[fragid*fragsizeshorts], fragsizebytes); 

				//COMPRESS data into the std::string snappyout
				snappy::Compress(snappyin.data(), snappyin.size(), &snappyout);

				//SEND
				udpcon[i].send((void*)snappyout.data(), snappyout.size());
				
				//printf("fragid:%d fragsizebytes:%d snappysize:%d\n", fragid, fragsizebytes, snappyout.size());
			}
#endif

#if defined(USE_RGB_IMAGE_STREAMS)
	                // Print Image first pixel
	                const ImageMetaData *imd = &sensors[i].imageMD;                  
	                const XnUInt8 *imageMap = sensors[i].imageMD.Data();
	                printf("Image frame [%d] first pixel is: R[%u],G[%u],B[%u]. FPS: %f\n", imd->FrameID(), imageMap[0], imageMap[1], imageMap[2], xnFPSCalc(&xnFPS));
#endif
	       	}
	}
	
	int i=0;
	for(i=0; i<numsensors; i++){
		sensors[i].depth.Release();
	}

	g_Context.Release();
	
   	return 0;
}
