#include "watch.h"

void watch::tic(){
	clock_gettime(CLOCK_MONOTONIC, &start);
}

timespec watch::toc(){
	clock_gettime(CLOCK_MONOTONIC, &stop);

	// calculate difference
	timespec temp;
	if((stop.tv_nsec-start.tv_nsec) < 0){
		temp.tv_sec = stop.tv_sec-start.tv_sec-1;
		temp.tv_nsec = 1000000000+stop.tv_nsec-start.tv_nsec;
	}
	else{
		temp.tv_sec = stop.tv_sec-start.tv_sec;
		temp.tv_nsec = stop.tv_nsec-start.tv_nsec;
	}

	return temp;
}