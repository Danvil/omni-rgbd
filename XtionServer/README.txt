The server software is preinstalled on the two Android-Minicomputers in the folder /home/picuntu/xtion-devel/XtionServer

To get everything running you have to make sure that the Accesspoint is working BEFORE you boot the android-computers. Otherwise they wont't connect automatically.
If you want to connect to a different network you have to modify /etc/wpa_supplicant.conf accordingly.

To boot the android-computers you have to make sure that the batteries are loaded (measure the voltage on the charging plug or use the battery-charger). Then you can press the on/off-switch.
When you can see a red stripe on the on/off-switch, the system is ON. You can also check this if you look at the android-computer carefully. They both have a blue LED inside their cases.

The Wifi-Dongles also have blue LEDs. You should be able to see them blinking if they connected correctly.

At the time of writing, the android-computers are configured to connect to the d-link dap2553 accesspoint that was given to me. Both computers get their ip address via dhcp and at the time of writing the ip addresses they get are 10.162.242.141 and 10.162.242.211.

The username on both is "picuntu" and the password is also "picuntu".
The root password is "12qwasyx".

You can connect to them by typing:
"ssh picuntu@10.162.242.141"
for example

To shut down the devices i recommend to send "sudo shutdown -h now" via ssh. After that the blue LEDs of the Android-computer should turn off (after 5 seconds) and then it is safe to press the on/off switch.


